package com.epam.newsmanagement.constant;

public class Constants {
	
	public static final String ALL_NEWS_PAGE = "jsp/clientAll.jsp";
	public static final String SINGLE_NEWS_PAGE = "jsp/clientSingle.jsp";
	public static final long NEWS_PER_PAGE = 3;

}
