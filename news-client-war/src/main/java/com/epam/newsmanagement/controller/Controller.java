package com.epam.newsmanagement.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsmanagement.command.ICommand;

@WebServlet(name = "Controller", urlPatterns = {""})
@SuppressWarnings("serial")
public class Controller extends HttpServlet {
	
	private static final String COMMAND = "command";

	private ApplicationContext context;

	public void init() throws ServletException {

		super.init();
		context = new ClassPathXmlApplicationContext("springApplicationContext.xml");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// Service service1 = (Service) context.getBean("service");

		ICommand command;

		String action = request.getParameter(COMMAND);

		
		if (action == null || action.isEmpty()) {
			command = (ICommand) context.getBean("view_all");
		} else {
			command = (ICommand) context.getBean(action);
		}
		
		

		String page = command.execute(request);

		request.getRequestDispatcher(page).forward(request, response);

	}

}