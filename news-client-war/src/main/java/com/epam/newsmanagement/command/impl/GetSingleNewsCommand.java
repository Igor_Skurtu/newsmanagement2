package com.epam.newsmanagement.command.impl;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.command.ICommand;
import com.epam.newsmanagement.constant.Constants;
import com.epam.newsmanagement.entity.NewsDTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.NewsDTOService;

@Component(value = "view_single")
public class GetSingleNewsCommand implements ICommand {

	private static final String NEWS_ID = "news_id";
	private static final String SINGLE_NEWS = "single_news";
	
	@Autowired
	private NewsDTOService newsDTOService;
	
	public String execute(HttpServletRequest request) {
		
		String page = Constants.SINGLE_NEWS_PAGE;
		
		long newsId = Long.parseLong(request.getParameter(NEWS_ID));
		
		NewsDTO news = null;
		
		try {
			news = newsDTOService.read(newsId);
		} catch (ServiceException e) {
			System.out.println(e);
		}
		
		request.getSession().setAttribute(SINGLE_NEWS, news);

		return page;
	}

}
