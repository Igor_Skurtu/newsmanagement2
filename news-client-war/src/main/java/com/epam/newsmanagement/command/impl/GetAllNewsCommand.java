package com.epam.newsmanagement.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.command.ICommand;
import com.epam.newsmanagement.constant.Constants;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.NewsDTOService;

@Component(value = "view_all")
public class GetAllNewsCommand implements ICommand {

	@Autowired
	private NewsDTOService newsDTOService;

	private static final String NEWS = "news";
	private static final String PAGE = "page";
	private static final String TOTAL_PAGES = "total";

	public String execute(HttpServletRequest request) {

		String page = Constants.ALL_NEWS_PAGE;
		long newsNumber = 0;
		long totalPages = 1;
		long pageNumber = 1;

		try {
			pageNumber = Long.parseLong(request.getParameter(PAGE));
		} catch (NumberFormatException e) {
			pageNumber = 1;
		}

		if (pageNumber <= 0) {
			pageNumber = 1;
		}

		List<News> newsList = null;

		try {
			newsNumber = newsDTOService.countAll();

			long from = Constants.NEWS_PER_PAGE * (pageNumber - 1) + 1;
			long to = from + Constants.NEWS_PER_PAGE - 1;

			newsList = newsDTOService.getFromTo(from, to);

			totalPages = (long) Math.ceil((double) newsNumber / (double) Constants.NEWS_PER_PAGE);

		} catch (ServiceException e) {
			System.out.println(e);
		}

		request.getSession().setAttribute(NEWS, newsList);

		request.getSession().setAttribute(TOTAL_PAGES, totalPages);

		request.getSession().setAttribute(PAGE, pageNumber);

		return page;
	}

}
