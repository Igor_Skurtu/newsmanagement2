package com.epam.newsmanagement.command.impl;

import java.sql.Timestamp;
import java.util.GregorianCalendar;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.command.ICommand;
import com.epam.newsmanagement.constant.Constants;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.NewsDTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.CommentService;
import com.epam.newsmanagement.service.impl.NewsDTOService;

@Component(value = "add_comment")
public class AddCommentCommand implements ICommand {
	
	private static final String NEWS_ID = "news_id";
	private static final String COMMENT_TEXT = "comment_text";
	
	@Autowired
	private NewsDTOService newsDTOService;
	
	@Autowired
	private CommentService commentService;

	public String execute(HttpServletRequest request) {
		
		String page = Constants.SINGLE_NEWS_PAGE;
		
		long newsId = Long.parseLong(request.getParameter(NEWS_ID));
		String commentText = request.getParameter(COMMENT_TEXT);
		
		Comment comment = new Comment();
		NewsDTO news = new NewsDTO();
		
		comment.setNewsID(newsId);
		comment.setCommentText(commentText);
		comment.setCreationDate(new Timestamp(new GregorianCalendar().getTimeInMillis()));
		
		try {
			commentService.create(comment);
			news = newsDTOService.read(newsId);
		} catch (ServiceException e) {
			System.out.println(e);
		}
		
		request.getSession().setAttribute("single_news", news);
		
		return page;
	}

}
