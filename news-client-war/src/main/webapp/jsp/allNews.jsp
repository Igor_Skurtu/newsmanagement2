<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=cp1251">
</head>
<body>

	<c:forEach var="news" items="${sessionScope.news}">

		<p>
			<a href="?command=view_single&news_id=${news.newsId}">${news.shortText}</a>
		</p>

	</c:forEach>

	<!-- PAGINATION -->
	<p align="center">

		<c:forEach var="i" begin="1" end="${sessionScope.total}">

			<c:choose>
				<c:when test="${i == sessionScope.page}">
				&nbsp;&nbsp;<a href="?command=view_all&page=${i}"><font size="+2"> <b>${i}</b></font> </a> &nbsp;&nbsp;
				</c:when>
				<c:when test="${i != sessionScope.page}">
				&nbsp;&nbsp;<a href="?command=view_all&page=${i}"> ${i} </a> &nbsp;&nbsp;
				</c:when>
			</c:choose>

		</c:forEach>

	</p>


</body>
</html>