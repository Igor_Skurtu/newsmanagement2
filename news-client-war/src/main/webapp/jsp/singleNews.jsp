<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=cp1251">
</head>
<body>

	${sessionScope.single_news.news.fullText}

	<c:forEach var="comment" items="${sessionScope.single_news.comments}">

		<p>${comment.commentText}</p>

	</c:forEach>

	<form action="?command=add_comment&news_id=${sessionScope.single_news.news.newsId}" method="POST">
		<textarea name="comment_text" rows="3" cols="50"></textarea>
		<button type="submit">Add comment</button>
	</form>

</body>
</html>