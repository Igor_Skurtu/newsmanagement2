package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.IRoleDAO;
import com.epam.newsmanagement.entity.Role;
import com.epam.newsmanagement.exception.DAOException;

public class RoleDAO implements IRoleDAO {

	private final String SQL_CREATE_ROLE = "INSERT INTO ROLES (ROLE_NAME) VALUES (?)"; //
	private final String SQL_READ_ROLE = "SELECT ROLE_ID, ROLE_NAME FROM ROLES WHERE ROLE_ID=?";
	private final String SQL_READ_ALL_ROLES = "SELECT ROLE_ID, ROLE_NAME FROM ROLES";
	private final String SQL_UPDATE_ROLE = "UPDATE ROLES SET ROLE_NAME=? WHERE ROLE_ID=?";
	private final String SQL_DELETE_ROLE = "DELETE FROM ROLES WHERE ROLE_ID=?";
	
	
	@Autowired
	private DataSource dataSource;


	public long create(Role t) throws DAOException {

		Connection connection = null;
		String[] column_names = { "ROLE_ID" }; // for Oracle database to prevent empty
											// ResultSet from getGeneratedKeys()
		Long roleId = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_ROLE, column_names);
			
			preparedStatement.setString(1, t.getRoleName());
			preparedStatement.executeUpdate();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			
			if (resultSet.next()) {
				roleId = resultSet.getLong(1);
			}
			
			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in RoleDAO.create()", e);

		} finally {
			 DataSourceUtils.releaseConnection(connection, dataSource);
		}

		return roleId;
	}

	public Role read(long roleId) throws DAOException {
		
		Role role = new Role();
		
		Connection connection = null;
		
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(SQL_READ_ROLE);
			
			preparedStatement.setLong(1, roleId);
			preparedStatement.execute();
			ResultSet resultSet = preparedStatement.getResultSet();
			
			if (resultSet.next()) {
				role.setRoleId(resultSet.getLong(1));
				role.setRoleName(resultSet.getString(2));
			}
			
			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in RoleDAO.read()", e);

		} finally {
			 DataSourceUtils.releaseConnection(connection, dataSource);
		}
		
		return role;
	}
	
	
	public List<Role> readAll() throws DAOException {
		
		List<Role> rolesList = new ArrayList<Role>();
		
		Connection connection = null;
		
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(SQL_READ_ALL_ROLES);

			preparedStatement.execute();
			ResultSet resultSet = preparedStatement.getResultSet();
			
			while (resultSet.next()) {
				Role role = new Role();
				role.setRoleId(resultSet.getLong(1));
				role.setRoleName(resultSet.getString(2));
				rolesList.add(role);
			}
			
			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in RoleDAO.readAll()", e);

		} finally {
			 DataSourceUtils.releaseConnection(connection, dataSource);
		}
		
		return rolesList;
	}


	public void update(Role t, long roleId) throws DAOException {
		
		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_ROLE);
			
			preparedStatement.setString(1, t.getRoleName());
			preparedStatement.setLong(2, roleId);
			preparedStatement.executeUpdate();
			
			
			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in RoleDAO.update()", e);

		} finally {
			 DataSourceUtils.releaseConnection(connection, dataSource);
		}


	}

	public void delete(long roleId) throws DAOException {
		
		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_ROLE);
			preparedStatement.setLong(1, roleId);
			preparedStatement.executeUpdate();
			
			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in RoleDAO.delete()", e);

		} finally {
			 DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}


}
