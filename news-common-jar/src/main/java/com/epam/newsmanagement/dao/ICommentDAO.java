package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Comment;

public interface ICommentDAO extends IDAO<Comment> {

}
