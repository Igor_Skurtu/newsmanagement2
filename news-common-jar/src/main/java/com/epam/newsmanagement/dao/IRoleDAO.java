package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Role;

public interface IRoleDAO extends IDAO<Role> {

}
