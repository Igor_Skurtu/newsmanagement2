package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.IUserDAO;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;

public class UserDAO implements IUserDAO {

	private final String CREATE_USER = "INSERT INTO USERS (ROLE_ID, USER_NAME, LOGIN, PASWORD) VALUES (?, ?, ?, ?)";
	private final String READ_USER = "SELECT USER_ID, ROLE_ID, USER_NAME, LOGIN, PASWORD FROM USERS WHERE USER_ID=?";
	private final String READ_USER_BY_NAME_AND_PASSWORD = "SELECT USER_ID, ROLES_ROLE_ID, USER_NAME, LOGIN, PASSWORD FROM USERS WHERE LOGIN=? AND PASSWORD=?";
	private final String READ_ALL_USERS = "SELECT USER_ID, ROLE_ID, USER_NAME, LOGIN, PASWORD FROM USERS";
	private final String UPDATE_USER = "UPDATE USERS SET ROLE_ID=?, USER_NAME=?, LOGIN=?, PASSWORD=? WHERE USER_ID=?";
	private final String DELETE_USER = "DELETE FROM USERS WHERE USER_ID=?";

	@Autowired
	private DataSource dataSource;

	public long create(User t) throws DAOException {
		Connection connection = null;
		String[] column_names = { "USER_ID" }; // for Oracle database to prevent
												// empty
												// ResultSet from
												// getGeneratedKeys()
		Long userId = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(CREATE_USER, column_names);

			preparedStatement.setLong(1, t.getRoleId());
			preparedStatement.setString(2, t.getUserName());
			preparedStatement.setString(3, t.getLogin());
			preparedStatement.setString(4, t.getPassword());
			preparedStatement.executeUpdate();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();

			if (resultSet.next()) {
				userId = resultSet.getLong(1);
			}

			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in UserDAO.create()", e);

		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

		return userId;
	}

	public User read(long userId) throws DAOException {
		User user = new User();

		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(READ_USER);

			preparedStatement.setLong(1, userId);
			preparedStatement.execute();
			ResultSet resultSet = preparedStatement.getResultSet();

			if (resultSet.next()) {
				user.setUserId(resultSet.getLong(1));
				user.setRoleId(resultSet.getLong(2));
				user.setUserName(resultSet.getString(3));
				user.setLogin(resultSet.getString(4));
				user.setPassword(resultSet.getString(5));
			}

			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in UserDAO.read()", e);

		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

		return user;
	}
	
	public User readByLoginAndPassword(String login, String password) throws DAOException {
		User user = null;

		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(READ_USER_BY_NAME_AND_PASSWORD);

			preparedStatement.setString(1, login);
			preparedStatement.setString(2, password);
			preparedStatement.execute();
			ResultSet resultSet = preparedStatement.getResultSet();

			if (resultSet.next()) {
				user = new User();
				user.setUserId(resultSet.getLong(1));
				user.setRoleId(resultSet.getLong(2));
				user.setUserName(resultSet.getString(3));
				user.setLogin(resultSet.getString(4));
				user.setPassword(resultSet.getString(5));
			}

			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in UserDAO.readByLoginAndPassword()", e);

		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

		return user;
	}

	public List<User> readAll() throws DAOException {
		List<User> usersList = new ArrayList<User>();
		
		Connection connection = null;
		
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(READ_ALL_USERS);

			preparedStatement.execute();
			ResultSet resultSet = preparedStatement.getResultSet();
			
			while (resultSet.next()) {
				User user = new User();
				user.setUserId(resultSet.getLong(1));
				user.setRoleId(resultSet.getLong(2));
				user.setUserName(resultSet.getString(3));
				user.setLogin(resultSet.getString(4));
				user.setPassword(resultSet.getString(5));
				usersList.add(user);
			}
			
			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in UserDAO.readAll()", e);

		} finally {
			 DataSourceUtils.releaseConnection(connection, dataSource);
		}
		
		return usersList;
	}

	public void update(User t, long userId) throws DAOException {
		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER);
			
			
			preparedStatement.setLong(1, t.getRoleId());
			preparedStatement.setString(2, t.getUserName());
			preparedStatement.setString(3, t.getLogin());
			preparedStatement.setString(4, t.getPassword());
			preparedStatement.setLong(5, userId);
			
			preparedStatement.executeUpdate();
			
			
			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in UserDAO.update()", e);

		} finally {
			 DataSourceUtils.releaseConnection(connection, dataSource);
		}

	}

	public void delete(long userId) throws DAOException {
		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(DELETE_USER);
			preparedStatement.setLong(1, userId);
			preparedStatement.executeUpdate();
			
			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in UserDAO.delete()", e);

		} finally {
			 DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

}
