package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Role;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;

public class NewsDAO implements INewsDAO {

	private final String CREATE_NEWS = "INSERT INTO NEWS (TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (?, ?, ?, ?, ?)";
	private final String READ_NEWS = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM NEWS WHERE NEWS_ID=?";
	private final String READ_ALL_NEWS = "SELECT NEWS_ID, TITLE, SHORT_TEXT, CREATION_DATE, MODIFICATION_DATE FROM NEWS";
	private final String READ_ALL_NEWS_FROM_TO = "SELECT * FROM ( SELECT T.*, ROWNUM MINR FROM ( SELECT NEWS_ID, TITLE, SHORT_TEXT, CREATION_DATE, MODIFICATION_DATE FROM NEWS ) T   WHERE ROWNUM<=? ) WHERE MINR>=?";
	private final String COUNT_ALL_NEWS = "SELECT COUNT(*) FROM NEWS";
	private final String READ_ALL_TAGS_IDS = "SELECT TAGS_TAG_ID FROM NEWS_TAG WHERE NEWS_NEWS_ID=?";
	private final String UPDATE_TAG = "UPDATE TAGS SET TAG_NAME=? WHERE TAG_ID=?";
	private final String DELETE_TAG_FROM_NEWS_TAG = "DELETE FROM NEWS_TAG WHERE TAGS_TAG_ID=?";
	private final String DELETE_TAG = "DELETE FROM TAGS WHERE TAG_ID=?";

	@Autowired
	private DataSource dataSource;

	public long create(News t) throws DAOException {
		Connection connection = null;
		String[] column_names = { "NEWS_ID" }; // for Oracle database to prevent
												// empty
												// ResultSet from
												// getGeneratedKeys()
		Long newsId = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(CREATE_NEWS, column_names);

			preparedStatement.setString(1, t.getTitle());
			preparedStatement.setString(2, t.getShortText());
			preparedStatement.setString(3, t.getFullText());
			preparedStatement.setTimestamp(4, t.getCreationDate());
			preparedStatement.setDate(4, t.getModificationDate());
			preparedStatement.executeUpdate();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();

			if (resultSet.next()) {
				newsId = resultSet.getLong(1);
			}

			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in NewsDAO.create()", e);

		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

		return newsId;
	}

	public News read(long newsId) throws DAOException {
		News news = new News();

		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(READ_NEWS);

			preparedStatement.setLong(1, newsId);
			preparedStatement.execute();
			ResultSet resultSet = preparedStatement.getResultSet();

			if (resultSet.next()) {
				news.setNewsId(resultSet.getLong(1));
				news.setTitle(resultSet.getString(2));
				news.setShortText(resultSet.getString(3));
				news.setFullText(resultSet.getString(4));
				news.setCreationDate(resultSet.getTimestamp(5));
				news.setModificationDate(resultSet.getDate(6));
			}

			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in TagDAO.read()", e);

		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

		return news;
	}

	public List<News> readAll() throws DAOException {

		List<News> newsList = new ArrayList<News>();

		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(READ_ALL_NEWS);

			preparedStatement.execute();
			ResultSet resultSet = preparedStatement.getResultSet();

			while (resultSet.next()) {
				News news = new News();
				news.setNewsId(resultSet.getLong(1));
				news.setTitle(resultSet.getString(2));
				news.setShortText(resultSet.getString(3));
				// news.setFullText(resultSet.getString(4));
				news.setCreationDate(resultSet.getTimestamp(4));
				news.setModificationDate(resultSet.getDate(5));
				newsList.add(news);
			}

			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in NewsDAO.readAll()", e);

		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

		return newsList;
	}
	
	public List<News> readAllFromTo(long from, long to) throws DAOException {

		List<News> newsList = new ArrayList<News>();

		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(READ_ALL_NEWS_FROM_TO);
			
			preparedStatement.setLong(1, to);
			preparedStatement.setLong(2, from);
			
			preparedStatement.execute();
			ResultSet resultSet = preparedStatement.getResultSet();

			while (resultSet.next()) {
				News news = new News();
				news.setNewsId(resultSet.getLong(1));
				news.setTitle(resultSet.getString(2));
				news.setShortText(resultSet.getString(3));
				// news.setFullText(resultSet.getString(4));
				news.setCreationDate(resultSet.getTimestamp(4));
				news.setModificationDate(resultSet.getDate(5));
				newsList.add(news);
			}

			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in NewsDAO.readAllFromTo()", e);

		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

		return newsList;
	}

	public long countAll() throws DAOException {

		Connection connection = null;
		long newsNumber = 0;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(COUNT_ALL_NEWS);

			preparedStatement.execute();
			ResultSet resultSet = preparedStatement.getResultSet();

			if (resultSet.next()) {
				newsNumber = resultSet.getLong(1);
			}

			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in NewsDAO.countAll()", e);

		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

		return newsNumber;
	}

	public void update(News t, long id) throws DAOException {
		// TODO Auto-generated method stub

	}

	public void delete(long id) throws DAOException {
		// TODO Auto-generated method stub

	}

}
