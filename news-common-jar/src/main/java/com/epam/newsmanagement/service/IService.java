package com.epam.newsmanagement.service;

import com.epam.newsmanagement.exception.ServiceException;


public interface IService<T> {

    long create(T element) throws ServiceException;

    T read(long elementId) throws ServiceException;

    void update (T element) throws ServiceException;

    void delete(T elementId) throws ServiceException;

}