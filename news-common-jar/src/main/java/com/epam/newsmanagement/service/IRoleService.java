package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.Role;

public interface IRoleService extends IService<Role> {

}
