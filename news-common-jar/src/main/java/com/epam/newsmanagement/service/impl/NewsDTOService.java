package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.impl.CommentDAO;
import com.epam.newsmanagement.dao.impl.NewsDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsDTO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.INewsDTOService;

public class NewsDTOService implements INewsDTOService {

	@Autowired
	private NewsDAO newsDAO;
	
	@Autowired
	private CommentDAO commentDAO;
	
	@Override
	public long create(NewsDTO element) throws ServiceException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public NewsDTO read(long newsId) throws ServiceException {
		
		try {
			NewsDTO newsDTO = new NewsDTO();
			News news = newsDAO.read(newsId);
			ArrayList<Comment> comments = (ArrayList<Comment>) commentDAO.readAllByNewsId(newsId);
			newsDTO.setNews(news);
			newsDTO.setComments(comments);
			return newsDTO;
		} catch (DAOException e) {
			throw new ServiceException("ServiceException in NewsService.read() ", e);
		}
	}
	
	@Override
	public List<News> readAll() throws ServiceException {
		
		try {
			List<News> newsList = newsDAO.readAll();
			return newsList;
		} catch (DAOException e) {
			throw new ServiceException("ServiceException in NewsService.readAll() ", e);
		}
	}

	@Override
	public List<News> getFromTo(long from, long to) throws ServiceException {
		
		try {
			List<News> newsList = newsDAO.readAllFromTo(from, to);
			return newsList;
		} catch (DAOException e) {
			throw new ServiceException("ServiceException in NewsService.getFromTo() ", e);
		}
		
	}

	@Override
	public void update(NewsDTO element) throws ServiceException {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(NewsDTO elementId) throws ServiceException {
		// TODO Auto-generated method stub

	}

	@Override
	public long countAll() throws ServiceException{
		
		long newsNumber = 0;
		try {
			newsNumber = newsDAO.countAll();
		} catch (DAOException e) {
			throw new ServiceException("ServiceException in NewsService.countAll() ", e);
		}
		return newsNumber;
	}



}
