package com.epam.newsmanagement.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.impl.UserDAO;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.IUserService;

public class UserService implements IUserService {

	@Autowired
	private UserDAO userDAO;
	
	@Override
	public long create(User element) throws ServiceException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public User read(long userId) throws ServiceException {
		
		User user = null;

		try {
			user = userDAO.read(userId);
			return user;
		} catch (DAOException e) {
			throw new ServiceException("ServiceException in UserService.read() ", e);
		}
	}
	
	@Override
	public User checkUser(String login, String passwordMd5) throws ServiceException {
		User user = null;

		try {
			user = userDAO.readByLoginAndPassword(login, passwordMd5);
		} catch (DAOException e) {
			throw new ServiceException("ServiceException in UserService.checkUser() ", e);
		}
		return user;
	}

	@Override
	public void update(User element) throws ServiceException {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(User elementId) throws ServiceException {
		// TODO Auto-generated method stub

	}

}
