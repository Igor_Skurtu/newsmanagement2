package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;;

public interface ITagService extends IService<Tag> {
	
	List<Tag> readAll() throws ServiceException;
}
