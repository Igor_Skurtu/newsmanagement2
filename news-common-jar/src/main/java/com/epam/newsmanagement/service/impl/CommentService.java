package com.epam.newsmanagement.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.impl.CommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.ICommentService;

public class CommentService implements ICommentService {

	@Autowired
	private CommentDAO commentDAO;
	
	@Override
	public long create(Comment comment) throws ServiceException {
		try {
			return commentDAO.create(comment);
		} catch (DAOException e) {
			throw new ServiceException("ServiceException in CommentService.create() ", e);
		}
	}

	@Override
	public Comment read(long elementId) throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(Comment element) throws ServiceException {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(Comment elementId) throws ServiceException {
		// TODO Auto-generated method stub

	}

}
