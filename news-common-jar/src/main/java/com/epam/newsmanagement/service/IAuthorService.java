package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.Author;

public interface IAuthorService extends IService<Author> {

}
