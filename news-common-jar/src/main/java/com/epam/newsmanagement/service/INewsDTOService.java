package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsDTO;
import com.epam.newsmanagement.exception.ServiceException;

public interface INewsDTOService extends IService<NewsDTO> {

	public long countAll() throws ServiceException;
	
	public List<News> readAll() throws ServiceException;
	
	public List<News> getFromTo(long from, long to) throws ServiceException;
}
