package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.impl.TagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.ITagService;

public class TagService implements ITagService {

	@Autowired
	private TagDAO tagDAO;
	
	@Override
	public long create(Tag element) throws ServiceException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Tag read(long tagId) throws ServiceException {
		
		Tag tag = null;

		try {
			tag = tagDAO.read(tagId);
			return tag;
		} catch (DAOException e) {
			throw new ServiceException("ServiceException in TagService.getTagById() ", e);
		}
	}
	
	@Override
	public List<Tag> readAll() throws ServiceException {
		
		try {
			List<Tag> tagList = tagDAO.readAll();
			return tagList;
		} catch (DAOException e) {
			throw new ServiceException("ServiceException in TagService.readAll() ", e);
		}
	}

	@Override
	public void update(Tag element) throws ServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Tag elementId) throws ServiceException {
		// TODO Auto-generated method stub
		
	}


}
