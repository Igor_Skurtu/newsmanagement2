package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.ServiceException;

public interface IUserService extends IService<User> {
	
	public User checkUser(String login, String passwordMd5) throws ServiceException;
}
