package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.Comment;

public interface ICommentService extends IService<Comment> {

}
