package com.epam.newsmanagement.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.impl.RoleDAO;
import com.epam.newsmanagement.entity.Role;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.IRoleService;

public class RoleService implements IRoleService {
	
	@Autowired
	private RoleDAO roleDAO;

	@Override
	public long create(Role element) throws ServiceException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Role read(long roleId) throws ServiceException {
		
		try {
			Role role = roleDAO.read(roleId);
			return role;
		} catch (DAOException e) {
			throw new ServiceException("ServiceException in RoleService.read() ", e);
		}
	}

	@Override
	public void update(Role element) throws ServiceException {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(Role elementId) throws ServiceException {
		// TODO Auto-generated method stub

	}

}
