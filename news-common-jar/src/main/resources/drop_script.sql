DROP TRIGGER AUTHORS_TRG;
DROP TRIGGER COMMENTS_TRG;
DROP TRIGGER NEWS_TRG;
DROP TRIGGER ROLES_TRG;
DROP TRIGGER TAGS_TRG;
DROP TRIGGER USERS_TRG;

DROP SEQUENCE AUTHORS_SEQ;
DROP SEQUENCE COMMENTS_SEQ;
DROP SEQUENCE NEWS_SEQ;
DROP SEQUENCE ROLES_SEQ;
DROP SEQUENCE TAGS_SEQ;
DROP SEQUENCE USERS_SEQ;

DROP TABLE NEWS_TAG;
DROP TABLE NEWS_AUTHOR;
DROP TABLE USERS;
DROP TABLE ROLES;
DROP TABLE COMMENTS;
DROP TABLE AUTHORS;
DROP TABLE TAGS;
DROP TABLE NEWS;