package com.epam.newsmanagement.exception;

import com.epam.newsmanagement.exception.ServiceException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ServiceException.class)
    public ModelAndView handleServiceException(Exception ex) {
        ModelAndView model = new ModelAndView();
        model.addObject("errorMessage", ex.getMessage());
        model.setViewName("error");
        return model;
    }
}
