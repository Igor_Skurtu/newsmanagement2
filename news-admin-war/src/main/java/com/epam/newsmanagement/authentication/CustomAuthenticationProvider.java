package com.epam.newsmanagement.authentication;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.RoleService;
import com.epam.newsmanagement.service.impl.UserService;

import java.util.List;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
	
	private static final String INVALID_LOGIN_MESSAGE = "Username or password is invalid";
	
    @Autowired
    private UserService userService;
    
    @Autowired
    private RoleService roleService;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		
        String login = authentication.getName();
        
        String password = authentication.getCredentials().toString();
        
        User user = null;
        String roleName = null;
        
        Md5PasswordEncoder encoder = new Md5PasswordEncoder();
        String md5Password = encoder.encodePassword(password, null);

        try {
            user = userService.checkUser(login, md5Password);
            
            if (user == null) {
                throw new BadCredentialsException(INVALID_LOGIN_MESSAGE);
            }
            
            long roleId = user.getRoleId();
            roleName = roleService.read(roleId).getRoleName(); 
            
        } catch (ServiceException e) {
        	throw new BadCredentialsException("TRY AGAIN LATER"); //temporary output
        }

        
        List<GrantedAuthority> grantedAuths = new ArrayList<>();        
        grantedAuths.add(new SimpleGrantedAuthority(roleName));
        return new UsernamePasswordAuthenticationToken(user.getUserName(), password, grantedAuths);
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}
