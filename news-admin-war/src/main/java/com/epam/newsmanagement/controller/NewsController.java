package com.epam.newsmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.NewsDTOService;
import com.epam.newsmanagement.constant.Constants;

@Controller 
@RequestMapping(value = "/news")
public class NewsController {
	
	@Autowired
	private NewsDTOService newsDTOService;
	
    @RequestMapping(value = "/{page}", method = RequestMethod.GET)
    public String index(Model model, @PathVariable("page") Long page) throws ServiceException {
    	
		long newsNumber = 0;
		long totalPages = 1;
		long pageNumber = 1;

		try {
			pageNumber = page;
		} catch (NumberFormatException e) {
			pageNumber = 1;
		}

		if (pageNumber <= 0) {
			pageNumber = 1;
		}

		List<News> newsList = null;

		try {
			newsNumber = newsDTOService.countAll();

			long from = Constants.NEWS_PER_PAGE * (pageNumber - 1) + 1;
			long to = from + Constants.NEWS_PER_PAGE - 1;

			newsList = newsDTOService.getFromTo(from, to);

			totalPages = (long) Math.ceil((double) newsNumber / (double) Constants.NEWS_PER_PAGE);

		} catch (ServiceException e) {
			System.out.println(e);
		}

		request.getSession().setAttribute(NEWS, newsList);

		request.getSession().setAttribute(TOTAL_PAGES, totalPages);

		request.getSession().setAttribute(PAGE, pageNumber);
    	
    	return "news_list";
    }

}
