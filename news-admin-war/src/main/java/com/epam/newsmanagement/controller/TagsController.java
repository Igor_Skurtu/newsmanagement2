package com.epam.newsmanagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.TagService;

@Controller 
@RequestMapping(value = "/tags")
public class TagsController {
	
	@Autowired
	private TagService tagService;
	
    @RequestMapping(method = RequestMethod.GET)
    public String login(Model model) {
    	return "tags";
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String viewTag(Model model, @PathVariable("id") Long tagId) throws ServiceException {
    	
    	Tag tag = tagService.read(tagId);
    	model.addAttribute("tag", tag);
        return "tags";
    }

}
