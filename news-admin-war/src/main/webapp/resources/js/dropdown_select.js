﻿var expanded = false;
function showCheckboxes() {
    var checkboxes = document.getElementById("checkboxes");
	var select = document.getElementsByClassName("selectBox")[0].getElementsByTagName("select")[0];
    if (!expanded) {
		select.blur();
        checkboxes.style.visibility = "visible";
        expanded = true;
    } else {
		select.blur();
        checkboxes.style.visibility = "hidden";
        expanded = false;
    }
}