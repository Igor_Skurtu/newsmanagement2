<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" session="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

	<h6>
		<security:authorize access="isAuthenticated()">
			<spring:message code="header.greeting" />
			<security:authentication property="principal" />
			<%--${admin-name}--%>
		</security:authorize>
	</h6>

	<!--	<input type="submit" value="Logout" /> -->
	
	        <security:authorize access="isAuthenticated()">
            <form action="<c:url value='/j_spring_security_logout'/>" method="POST" class="hello_admin">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <input type="submit" value=<spring:message code="header.logout"/> />
            </form>
        </security:authorize>

<h1>News Portal - Administration</h1>
<div class="lang_container">
	<a href="?lang=en" class="lang">EN</a><a href="?lang=ru" class="lang">RU</a>
</div>
<hr color="#18A0C9" width="100%" />