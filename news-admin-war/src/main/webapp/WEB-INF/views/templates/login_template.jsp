<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



	<!DOCTYPE html>
	<html>
<head>
<meta charset="utf-8">
<meta name="description" content="News portal">
<meta http-equiv="keywords"
	content="news, portal, hot news, breaking news">
<!-- CSS -->
<link rel="stylesheet" href="resources/css/style.css">
<script type="text/javascript" src="resources/js/dropdown_select.js"></script>
<title>News Portal - Login</title>
</head>
<body>
	<header>

		<tiles:insertAttribute name="header" />

	</header>

		<main> <tiles:insertAttribute name="body" /> </main>

	<footer>
		<tiles:insertAttribute name="footer" />
	</footer>
</body>
	</html>