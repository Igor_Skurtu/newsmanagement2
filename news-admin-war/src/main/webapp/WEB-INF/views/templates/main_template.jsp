<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>



	<!DOCTYPE html>
	<html>
<head>
<meta charset="utf-8">
<meta name="description" content="News portal">
<meta http-equiv="keywords"
	content="news, portal, hot news, breaking news">

   <spring:url value="/resources/css/style.css" var="style"/>
   <spring:url value="/resources/js/dropdown_select.js" var="dropdown"/>


    <link rel="stylesheet" href="${style}" type="text/css" />
    <script type="text/javascript" src="${dropdown}"></script>


<title>News Portal - Admin</title>
</head>
<body>
	<header>

		<tiles:insertAttribute name="header" />

	</header>
	<table class="admin_container">
		<tbody>
			<tr>
				<td valign="top"><tiles:insertAttribute name="menu" /></td>
				<td><main> <tiles:insertAttribute name="body" /> </main></td>
			</tr>
		</tbody>
	</table>
	<footer>
		<tiles:insertAttribute name="footer" />
	</footer>
</body>
	</html>